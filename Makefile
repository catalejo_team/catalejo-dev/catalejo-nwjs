# nwjs-version=v0.60.0
nwjs-version=v0.73.0
nwjs-ffmpeg-v=0.73.0
nwjs-ffmpeg-release=https://github.com/iteufel/nwjs-ffmpeg-prebuilt/releases/download/$(nwjs-ffmpeg-v)/$(nwjs-ffmpeg-v)
nwjs-win32=nwjs-$(nwjs-version)-win-ia32
nwjs-linux64=nwjs-$(nwjs-version)-linux-x64
# sdk version
nwjs-sdk-linux64=nwjs-sdk-$(nwjs-version)-linux-x64

nwjs-win32-d=https://dl.nwjs.io/$(nwjs-version)/$(nwjs-win32).zip
nwjs-linux64-d=https://dl.nwjs.io/$(nwjs-version)/$(nwjs-linux64).tar.gz
# sdk-version
nwjs-sdk-linux64-d=https://dl.nwjs.io/$(nwjs-version)/$(nwjs-sdk-linux64).tar.gz
# chuck application
chuck-win32-d=https://sourceforge.net/projects/chuck-linux-win-catalejo/files/chuck.zip
chuck-linux64-d=https://sourceforge.net/projects/chuck-linux-win-catalejo/files/chuck.tar.gz
# civetweb application
civetweb-win32=CivetWeb_Win32\+64_V1.15.zip
civetweb-win32-d=https://sourceforge.net/projects/civetweb/files/1.15/$(civetweb-win32)
civetweb-linux64=civetweb-1.16
civetweb-linux64-d=https://sourceforge.net/projects/civetweb/files/1.16/$(civetweb-linux64).tar.gz

help:
	@echo "help"

### DEPLY CATALEJO EDITOR (Win 32) ###
win32-download-nwjs:
	mkdir -p nwjs-precompiled
	cd nwjs-precompiled && curl -L -O $(nwjs-win32-d)
	cd nwjs-precompiled && curl -L -O $(nwjs-ffmpeg-release)-win-ia32.zip
win32-unpack-nwjs:
	mkdir -p build-nwjs/$(nwjs-win32)
	rm -rf build-nwjs/$(nwjs-win32)
	cd ./nwjs-precompiled/ && unzip $(nwjs-win32).zip -d ../build-nwjs/
	cd ./nwjs-precompiled/ && unzip -o $(nwjs-ffmpeg-v)-win-ia32.zip -d ../build-nwjs/$(nwjs-win32)/
win32-chuck-unpack:
	mkdir -p chuck-precompiled
	cd chuck-precompiled && curl -L -O $(chuck-win32-d)
	mkdir -p build-nwjs/$(nwjs-win32)
	cd ./chuck-precompiled/ && unzip chuck.zip -d ../build-nwjs/$(nwjs-win32)/
win32-civetweb-unpack:
	mkdir -p civetweb-precompiled
	cd civetweb-precompiled && curl -L -O $(civetweb-win32-d)
	mkdir -p build-nwjs/$(nwjs-win32)/civetweb
	cd ./civetweb-precompiled/ && unzip $(civetweb-win32) && cp -var CivetWeb/CivetWeb32.exe ../build-nwjs/$(nwjs-win32)/civetweb/
win32-config-catalejo:
	cp ./nwjs-catalejo-package.json ./build-nwjs/$(nwjs-win32)/package.json
win32-deploy-catalejo:
	cd ../catalejo-desktop/catalejo-vue3-desktop/ && make b && echo "Se realiza actualización de dist/"
	rm -rf ./build-nwjs/$(nwjs-win32)/dist
	cp -var ../catalejo-desktop/catalejo-vue3-desktop/dist ./build-nwjs/$(nwjs-win32)/
	sed -i "s!./dist/img/!./img/!g" ./build-nwjs/$(nwjs-win32)/dist/assets/*.js # TODO
win32-package-catalejo:
	cd ./build-nwjs/ && rm -rf catalejo-win32 && sync
	cd ./build-nwjs/ && cp -var $(nwjs-win32) catalejo-win32 && sync
	cd ./build-nwjs/ && zip -r catalejo-win32.zip catalejo-win32 && sync
win32-release-catalejo: win32-download-nwjs win32-unpack-nwjs win32-chuck-unpack win32-civetweb-unpack win32-config-catalejo win32-deploy-catalejo win32-package-catalejo

### DEPLOY CATALEJO EDITOR (Linux 64) ###
linux64-download-nwjs:
	mkdir -p nwjs-precompiled 
	(cd nwjs-precompiled && curl -L -O $(nwjs-linux64-d) || echo "NO SE PUEDO DESCARGAR NWJS VERSION DE LINUX64")
	(cd nwjs-precompiled && curl -L -O $(nwjs-ffmpeg-release)-linux-x64.zip && unzip -t $(nwjs-ffmpeg-v)-linux-x64.zip || echo "NO SE PUDO DESCARGAR NWJS-FFMPEG VERSION LINUX64")
linux64-unpack-nwjs:
	mkdir -p build-nwjs/$(nwjs-linux64)
	rm -rf build-nwjs/$(nwjs-linux64)
	tar xvf ./nwjs-precompiled/$(nwjs-linux64).tar.gz -C build-nwjs/
	cd ./nwjs-precompiled/ && unzip -o $(nwjs-ffmpeg-v)-linux-x64.zip -d ../build-nwjs/$(nwjs-linux64)/lib/
linux64-chuck-unpack:
	mkdir -p chuck-precompiled
	cd chuck-precompiled && curl -L -O $(chuck-linux64-d)
	mkdir -p build-nwjs/$(nwjs-linux64)
	tar xvf ./chuck-precompiled/chuck.tar.gz -C build-nwjs/$(nwjs-linux64)/
linux64-civetweb-build:
	mkdir -p civetweb-precompiled
	cd civetweb-precompiled && curl -L -O $(civetweb-linux64-d)
	mkdir -p build-nwjs/$(nwjs-linux64)/civetweb
	tar xvf ./civetweb-precompiled/$(civetweb-linux64).tar.gz -C ./civetweb-precompiled/
	cd ./civetweb-precompiled/$(civetweb-linux64) && make build
	cp -var ./civetweb-precompiled/$(civetweb-linux64)/civetweb build-nwjs/$(nwjs-linux64)/civetweb/
linux64-config-catalejo:
	cp ./nwjs-catalejo-package.json ./build-nwjs/$(nwjs-linux64)/package.json
linux64-deploy-catalejo:
	cd ../catalejo-desktop/catalejo-vue3-desktop/ && make b && echo "Se realiza actualización de dist/"
	rm -rf ./build-nwjs/$(nwjs-linux64)/dist
	cp -var ../catalejo-desktop/catalejo-vue3-desktop/dist ./build-nwjs/$(nwjs-linux64)/
	sed -i "s!./dist/img/!./img/!g" ./build-nwjs/$(nwjs-linux64)/dist/assets/*.js # TODO
linux64-package-catalejo:
	cd ./build-nwjs/ && rm -rf catalejo-linux64 && sync
	cd ./build-nwjs/ && cp -var $(nwjs-linux64) catalejo-linux64 && sync
	cd ./build-nwjs/ && tar -czvf catalejo-linux64.tar.gz catalejo-linux64 && sync
linux64-release-catalejo: linux64-download-nwjs linux64-unpack-nwjs linux64-chuck-unpack linux64-civetweb-build linux64-config-catalejo linux64-deploy-catalejo linux64-package-catalejo

### DEBUG SDK (Linux 64) ###
# 1. Descargar nwjs en su versión sdk
linux64-download-nwjs-sdk:
	mkdir -p nwjs-precompiled 
	(cd nwjs-precompiled && curl -L -O $(nwjs-sdk-linux64-d) || echo "NO SE PUEDO DESCARGAR NWJS VERSION DE LINUX64")
	(cd nwjs-precompiled && curl -L -O $(nwjs-ffmpeg-release)-linux-x64.zip && unzip -t $(nwjs-ffmpeg-v)-linux-x64.zip || echo "NO SE PUDO DESCARGAR NWJS-FFMPEG VERSION LINUX64")
# 2. Desempaquetar nwjs en su versión sdk y demás paquetes
linux64-unpack-nwjs-sdk:
	mkdir -p build-nwjs/$(nwjs-sdk-linux64)
	rm -rf build-nwjs/$(nwjs-sdk-linux64)
	tar xvf ./nwjs-precompiled/$(nwjs-sdk-linux64).tar.gz -C build-nwjs/
	cd ./nwjs-precompiled/ && unzip -o $(nwjs-ffmpeg-v)-linux-x64.zip -d ../build-nwjs/$(nwjs-sdk-linux64)/lib/
# 3. Descargar y desempaquetar complementos
# 3.1 Chuck
linux64-chuck-unpack-sdk:
	mkdir -p chuck-precompiled
	cd chuck-precompiled && curl -L -O $(chuck-linux64-d)
	mkdir -p build-nwjs/$(nwjs-sdk-linux64)
	tar xvf ./chuck-precompiled/chuck.tar.gz -C build-nwjs/$(nwjs-sdk-linux64)/
# 3.2 civetweb
linux64-civetweb-build-sdk:
	mkdir -p civetweb-precompiled
	cd civetweb-precompiled && curl -L -O $(civetweb-linux64-d)
	mkdir -p build-nwjs/$(nwjs-sdk-linux64)/civetweb
	tar xvf ./civetweb-precompiled/$(civetweb-linux64).tar.gz -C ./civetweb-precompiled/
	cd ./civetweb-precompiled/$(civetweb-linux64) && make build
	cp -var ./civetweb-precompiled/$(civetweb-linux64)/civetweb build-nwjs/$(nwjs-sdk-linux64)/civetweb/
# 4. Agregar package.json para habilitar el debug remoto a través de vitejs
# (esto se hace una única vez).
linux64-config-catalejo-sdk:
	# crear paquete de herramientas toolbox blockly para luna8266 y chuck
	cd ../catalejo-oursblockly/targets/luna8266/ && ./make_pack.sh desktop luna8266
	cd ../catalejo-oursblockly/targets/chuck/ && ./make_pack.sh desktop chuck
	cp ./nwjs-sdk-catalejo-package.json ./build-nwjs/$(nwjs-sdk-linux64)/package.json # permite el uso de vitejs
# Instalar las herramientas para el debug
linux64-debug-install: linux64-download-nwjs-sdk linux64-unpack-nwjs-sdk linux64-chuck-unpack-sdk linux64-civetweb-build-sdk
# 5. Iniciar el servicio de vitejs con debug para usar en el ambiente de nwjs (consola y variables) sin virtualizar
vitejs: linux64-config-catalejo-sdk
	cd ../catalejo-desktop/catalejo-vue3-desktop/ && make d && echo "Se lanza el servicio de Vitejs"
# 6. Lanzar nwjs version sdk para hacer seguimiento a la aplicación.
linux64-run-nwjs-sdk:
	cd ./build-nwjs/$(nwjs-sdk-linux64)/ && ./nw &
	echo "Se lanza el ./nw en background process"
d: vitejs
r: linux64-run-nwjs-sdk

