# Catalejo Editor nw-js

## Deploy application

### Linux 64 bits

1. Descargar y desempaquetar

```bash
make linux64-download-nwjs linux64-unpack-nwjs
```

2. Configurar nwjs y construir aplicación Catalejo Editor

```bash
make linux64-config-catalejo linux64-deploy-catalejo
```

3. Empaquetar

```bash
make linux64-package-catalejo
```

* Único comando: `make linux64-release-catalejo`

### Windows 32 bits

```bash
make win32-download-nwjs win32-unpack-nwjs win32-chuck-unpack win32-config-catalejo win32-deploy-catalejo
make win32-package-catalejo
```

* Único comando: `make win32-release-catalejo`

## NWJS SDK DEBUG

1. Descargar y configura nwjs sdk:

```bash
make linux64-download-nwjs-sdk linux64-unpack-nwjs-sdk
```

2. Instalar chuck y civetweb

```bash
make linux64-chuck-unpack-sdk linux64-civetweb-build-sdk
```

Observaciones: el paso 1. y 2. se puede instalar con el siguiente comando:
```
make linux64-debug-install
```

3. Configuración nwjs-sdk y complementos más debug: Lanzar en terminales separadas los siguientes comandos

* 3.1 Lanzar Vitejs

```
make d
```

* 3.2 Lanzar nwjs-sdk

```bash
make r
```
